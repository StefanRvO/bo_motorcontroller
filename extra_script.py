from os import makedirs
from os.path import isdir, join, dirname, basename
import zlib
import struct
import subprocess
Import('env')


def create_output(target, source, env):
    subprocess.run([env["OBJCOPY"], "-O", "binary", "-R", ".eeprom", env.subst("$BUILD_DIR/${PROGNAME}.elf"), env.subst("$BUILD_DIR/${PROGNAME}_crc.bin")])

    data = open(env.subst("$BUILD_DIR/${PROGNAME}_crc.bin"), "rb").read()[:0x400 * 88 - 4]
    crc = zlib.crc32(data)
    with open(env.subst("$BUILD_DIR/${PROGNAME}_crc.bin"), "wb") as f:
        f.write(data)
        print("Appending CRC: ", env.subst("$BUILD_DIR/${PROGNAME}_crc.bin"), hex(crc))
        f.write(struct.pack("<I", crc))
        f.flush()

    with open(env.subst("$BUILD_DIR/speaker_fw.bin"), "wb") as f:
        f.write(data)
        f.write(struct.pack("<I", crc))
        f.flush()



env.AddPostAction("$BUILD_DIR/${PROGNAME}.elf", create_output)

env.AddPostAction(
    "$BUILD_DIR/${PROGNAME}.elf",
    env.VerboseAction(" ".join([
        "$OBJCOPY", "-O", "ihex", "-R", ".eeprom",
        "$BUILD_DIR/${PROGNAME}.elf", "$BUILD_DIR/${PROGNAME}.hex"
    ]), "Building $BUILD_DIR/${PROGNAME}.hex")
)


    
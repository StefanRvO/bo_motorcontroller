#include <inttypes.h>

#include "BOSpeakerControl/BOSpeakerControl.hpp"
#include "BOSpeakerMotor/BOSpeakerMotor.hpp"
#include "LEDControl/LEDControl.hpp"
#include "bo_pins.h"
#include "common/Bootloader/Bootloader.hpp"
#include "common/CANMessages.hpp"
#include "common/CPULoadTimer/CPULoadTimer.hpp"
#include "common/DeviceStats/DeviceStats.hpp"
#include "common/EventTimer/EventTimer.hpp"
#include "common/PinStateReader/PinStateReader.hpp"
#include "common/TJA1055/TJA1055.hpp"
#include "common/setFrequency/setFrequency.hpp"
#include "drv8841/drv8841.hpp"
#include "mbed.h"
#include "unknownpins.hpp"

__attribute__((section("AHBSRAM0"))) static uint8_t MOTOR_CONTROL_STACK[2048];

static ledcontrol::LEDControl led(BO_LED_PIN);

static float    motor_speed   = 1;
static uint16_t pwm_frequency = 30000;

static drv8841::BDCMotor m1(BO_DRV_M0_IN1, BO_DRV_M0_IN2, BO_DRV_M0_CURRENT0, BO_DRV_M0_CURRENT1, motor_speed);
static drv8841::BDCMotor m2(BO_DRV_M1_IN1, BO_DRV_M1_IN2, BO_DRV_M1_CURRENT0, BO_DRV_M1_CURRENT1, motor_speed);

static speakermotor::BOSpeakerMotor
  updownMotor(m1, std::vector<PinName>{BO_P3_SW1_START}, std::vector<PinName>{BO_P3_SW2_END});

static speakermotor::BOSpeakerMotor
  unfoldMotor(m2, std::vector<PinName>{BO_P5_SW3_START, BO_P5_SW4_START}, std::vector<PinName>{BO_P5_SW1_END, BO_P5_SW2_END});

static speakermotor::BOSpeakerControl control(updownMotor, unfoldMotor);
static drv8841::DRV8841Config         motorConfig(BO_DRV_DECAY, BO_DRV_RESET, BO_DRV_SLEEP, BO_DRV_FAULT);

static TJA1055      can(CAN_RX, CAN_TX, CAN_ERR_N, CAN_STB, CAN_EN);
static CPULoadTimer load_timer(LPC_TIM0);
static EventTimer   event_timer;

static void publishCRC()
{
  uint8_t data[4];
  *(reinterpret_cast<uint32_t*>(data + 0)) = __builtin_bswap32(Bootloader::get_crc_from_flash());

  can.write(mbed::CANMessage(static_cast<unsigned int>(CanMessageType::SPEAKER_APPLICATION_CRC), data, sizeof(data)));
}

static void publishStatus()
{
  uint8_t data[8];
  data[0]                                  = static_cast<uint8_t>(control.getState());
  data[1]                                  = static_cast<uint8_t>(control.getTarget());
  *(reinterpret_cast<float*>(data + 2))    = motor_speed;
  *(reinterpret_cast<uint16_t*>(data + 6)) = pwm_frequency;

  can.write(mbed::CANMessage(static_cast<unsigned int>(CanMessageType::SPEAKER_CONTROL_STATUS), data, sizeof(data)));
}

static void motor_task()
{
  uint64_t nextWake = get_ms_count();

  while(1)
  {
    load_timer.startLoad();
    Watchdog::get_instance().kick();
    nextWake += 1;
    control.service();
    led.service(control.getState());
    can.recieveMessages();
    event_timer.step();
    load_timer.endLoad();
    thread_sleep_until(nextWake);
  }
}

static void resetCallback([[maybe_unused]] CANMessage& msg)
{
  NVIC_SystemReset();
}

static void commandCallback(CANMessage& msg)
{
  control = static_cast<speakermotor::BOSpeakerControlTarget>(msg.data[0]);
  if(msg.len >= 5)
  {
    motor_speed = *reinterpret_cast<float*>(msg.data + 1);
    motor_speed = std::min(1.f, motor_speed);
    motor_speed = std::max(0.f, motor_speed);
  }
  if(msg.len >= 7)
  {
    uint16_t frequency = *reinterpret_cast<uint16_t*>(msg.data + 5);
    frequency          = std::min((uint16_t)65000U, frequency);
    frequency          = std::max((uint16_t)50U, frequency);
    if(frequency != pwm_frequency)
    {
      pwm_frequency = frequency;
      m1.setPWMFreq(pwm_frequency);
      m2.setPWMFreq(pwm_frequency);
    }
  }
}

__attribute__((used)) int main()
{
  setFrequency();

  m1.setPWMFreq(30000);
  m2.setPWMFreq(30000);

  motorConfig.setReset(0);
  motorConfig.setSleep(0);

  event_timer.addEvent(publishStatus, get_ms_count() + 0, 50);
  event_timer.addEvent(publishCRC, get_ms_count() + 5, 100);
  event_timer
    .addEvent([]() { DeviceStats::sendHeap(can, CanMessageType::SPEAKER_DEVICE_STATS_HEAP); }, get_ms_count() + 10, 500);
  event_timer.addEvent(
    []() { DeviceStats::sendLoad(can, load_timer, CanMessageType::SPEAKER_DEVICE_STATS_LOAD); },
    get_ms_count() + 15,
    500);
  event_timer.addEvent(
    []() { DeviceStats::sendUptime(can, CanMessageType::SPEAKER_DEVICE_STATS_UPTIME); },
    get_ms_count() + 25,
    500);

  const uint32_t WDOG_TIMEOUT_MS = 2500;
  Watchdog&      watchdog        = Watchdog::get_instance();
  watchdog.start(WDOG_TIMEOUT_MS);

  can.registerCallback(CanMessageType::SPEAKER_CONTROL_REBOOT, resetCallback);
  can.registerCallback(CanMessageType::SPEAKER_CONTROL_COMMAND, commandCallback);

  Thread* motor_thread =
    new Thread(osPriorityNormal, sizeof(MOTOR_CONTROL_STACK), MOTOR_CONTROL_STACK, "control_thread");
  motor_thread->start(motor_task);

  while(1)
  {
    thread_sleep_for(1000000);
  }

  return 0;
}
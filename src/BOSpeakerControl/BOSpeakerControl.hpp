#pragma once
#include "BOSpeakerMotor/BOSpeakerMotor.hpp"
#include "common/SpeakerTypes.hpp"

namespace speakermotor
{

class BOSpeakerControl {
 public:
  BOSpeakerControl(BOSpeakerMotor& updown, BOSpeakerMotor& unfold);
  void                   service();
  BOSpeakerControlState  getState();
  BOSpeakerControlTarget getTarget() { return target; }

  BOSpeakerControl& operator=(BOSpeakerControlTarget val)
  {
    target = val;
    return *this;
  }

 private:
  BOSpeakerControlTarget target;
  BOSpeakerMotor&        updownMotor;
  BOSpeakerMotor&        unfoldMotor;
  uint32_t               shouldMove = 0;
};
}  // namespace speakermotor
#include "BOSpeakerControl.hpp"

namespace speakermotor
{
BOSpeakerControl::BOSpeakerControl(BOSpeakerMotor& updown, BOSpeakerMotor& unfold)
  : target(BOSpeakerControlTarget::FOLD_IN),
    updownMotor(updown),
    unfoldMotor(unfold)
{
}

BOSpeakerControlState BOSpeakerControl::getState()
{
  switch(target)
  {
    case BOSpeakerControlTarget::UNFOLD:
      if(!(updownMotor.atEnd() && unfoldMotor.atEnd()))
      {
        return BOSpeakerControlState::UNFOLDING;
      }
      return BOSpeakerControlState::UNFOLDED;
    case BOSpeakerControlTarget::FOLD_IN:
      if(!(updownMotor.atStart() && unfoldMotor.atStart()))
      {
        return BOSpeakerControlState::FOLDING;
      }
      return BOSpeakerControlState::FOLDED;
    case BOSpeakerControlTarget::STOP:
    default:
      if(updownMotor.atStart() && unfoldMotor.atStart())
      {
        return BOSpeakerControlState::FOLDED;
      }
      if(updownMotor.atEnd() && unfoldMotor.atEnd())
      {
        return BOSpeakerControlState::UNFOLDED;
      }
      return BOSpeakerControlState::STOPPED;
  }
}

void BOSpeakerControl::service()
{
  updownMotor.service();
  unfoldMotor.service();

  switch(target)
  {
    case BOSpeakerControlTarget::STOP:
      updownMotor = BOSpeakerMotorTarget::STOP;
      unfoldMotor = BOSpeakerMotorTarget::STOP;
      break;
    case BOSpeakerControlTarget::UNFOLD:
      if(!updownMotor.atEnd())
      {
        updownMotor = BOSpeakerMotorTarget::END;
        unfoldMotor = BOSpeakerMotorTarget::START;
        break;
      }
      if(!unfoldMotor.atEnd())
      {
        updownMotor = BOSpeakerMotorTarget::END;
        unfoldMotor = BOSpeakerMotorTarget::END;
        break;
      }
      break;
    case BOSpeakerControlTarget::FOLD_IN:
    default:
      if(!unfoldMotor.atStart())
      {
        updownMotor = BOSpeakerMotorTarget::END;
        unfoldMotor = BOSpeakerMotorTarget::START;
        break;
      }

      if(!updownMotor.atStart())
      {
        updownMotor = BOSpeakerMotorTarget::START;
        unfoldMotor = BOSpeakerMotorTarget::START;
        break;
      }
      break;
  }
}

}  // namespace speakermotor
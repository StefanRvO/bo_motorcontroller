#pragma once
#include "common/bo_pins.h"
#include "mbed.h"

// Misc
#define BO_LED_PIN P2_0  // Pin 60

// Texas Instruments DRV8841 Dual BDC driver
#define BO_DRV_M0_IN1      P1_18  // Motor 0, input 1
#define BO_DRV_M0_IN2      P1_20  // Motor 0, input 2
#define BO_DRV_M0_CURRENT0 P1_22  // Motor 0, current set 0
#define BO_DRV_M0_CURRENT1 P1_25  // Motor 0, current set 1

#define BO_DRV_M1_IN1      P1_23  // Motor 1, input 1
#define BO_DRV_M1_IN2      P1_24  // Motor 1, input 2
#define BO_DRV_M1_CURRENT0 P1_28  // Motor 1, current set 0
#define BO_DRV_M1_CURRENT1 P1_29  // Motor 1, current set 1

#define BO_DRV_DECAY P0_22  // Decay mode
#define BO_DRV_SLEEP P0_17  // Sleep
#define BO_DRV_FAULT P0_15  // Fault

#define BO_DRV_RESET P0_18  // Reset

// NXP TJA1055/3 CAN Transciever
#define B0_CAN_TJA1055_TXD P0_1
#define B0_CAN_TJA1055_RXD P0_0
#define B0_CAN_TJA1055_ERR P1_9
#define B0_CAN_TJA1055_STB P2_9
#define B0_CAN_TJA1055_EN  P2_8

// LV4051 8-channel Analog Mux/Demux (TI or NXP)

// Limit switches
// P3
#define BO_P3_SW1_START P0_16  // Pin 48
#define BO_P3_SW2_END   P2_7   // Pin 51

// P5
// P2_6 doesn't seem to change when hitting the end, but my guess is that there is two switches
// on each end for redundency/tolerance reasons, It seems to fit when i trace the connection.
#define BO_P5_SW1_END   P2_6
#define BO_P5_SW2_END   P2_5
#define BO_P5_SW3_START P2_4
#define BO_P5_SW4_START P2_3

// P4
#define BO_P4_PIN3 P0_2
#define BO_P4_PIN2 PO_3

// P6
#define BO_P6_PIN2 P1_26
#define BO_P6_PIN3 P1_19

// Test points
#define BO_TESTPOINT_MOVE_TRIGGER P0_9

// Chips not connected to the MCU / Not used:

// ST LM324 OP-Amp - Current feedback for DRV8841
#include "LEDControl.hpp"

using namespace speakermotor;

namespace ledcontrol
{
LEDControl::LEDControl(PinName pin) : led(pin)
{
  blinkPatterns[static_cast<uint32_t>(BOSpeakerControlState::FOLDED)]    = 0xF1FF0000F1FF0000;
  blinkPatterns[static_cast<uint32_t>(BOSpeakerControlState::UNFOLDED)]  = 0xFF00FF00FF00FF00;
  blinkPatterns[static_cast<uint32_t>(BOSpeakerControlState::UNFOLDING)] = 0xFFF1FF7FF1FF0000;
  blinkPatterns[static_cast<uint32_t>(BOSpeakerControlState::FOLDING)]   = 0x000000000000FFFF;
  blinkPatterns[static_cast<uint32_t>(BOSpeakerControlState::STOPPED)]   = 0x00000000F1F1F1F1;
}

void LEDControl::service(speakermotor::BOSpeakerControlState state)
{
  const auto pattern = blinkPatterns[static_cast<uint32_t>(state)];

  intervalCounter = (intervalCounter + 1) % STEP_INTERVAL;
  if(intervalCounter % STEP_INTERVAL == 0)
  {
    stepCounter = (stepCounter + 1) % (sizeof(pattern) * 8);
    led         = (~pattern >> stepCounter) & 1;
  }
}

}  // namespace ledcontrol
#pragma once
#include "BOSpeakerControl/BOSpeakerControl.hpp"
#include "mbed.h"
#include <array>

namespace ledcontrol
{
class LEDControl {
 public:
  LEDControl(PinName pin);
  void service(speakermotor::BOSpeakerControlState state);

 private:
  DigitalOut                                                                            led;
  static constexpr uint32_t                                                             STEP_INTERVAL   = 15;
  std::array<uint64_t, static_cast<uint32_t>(speakermotor::BOSpeakerControlState::CNT)> blinkPatterns   = {0};
  uint8_t                                                                               stepCounter     = 0;
  uint32_t                                                                              intervalCounter = 0;
};
}  // namespace ledcontrol
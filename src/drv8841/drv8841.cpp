#include "drv8841.hpp"

namespace drv8841
{

BDCMotor::BDCMotor(PinName _In1, PinName _In2, PinName _Iset0, PinName _Iset1, float& speed)
  : In1(_In1),
    In2(_In2),
    Iset0(_Iset0, 1),
    Iset1(_Iset1, 1),
    speed(speed)
{
}

void BDCMotor::setDirection(MotorDirection direction)
{
  CriticalSectionLock lock;
  if(direction == MotorDirection::Forward || direction == MotorDirection::Reverse)
  {
    In1 = (static_cast<int>(direction) & 1) * speed;
    In2 = ((static_cast<int>(direction) >> 1) & 1) * speed;
  }
  else
  {
    In1 = static_cast<int>(direction) & 1;
    In2 = (static_cast<int>(direction) >> 1) & 1;
  }
}

void BDCMotor::setCurrent(MotorCurrent current)
{
  Iset0 = static_cast<int>(current) & 1;
  Iset1 = (static_cast<int>(current) >> 1) & 1;
}

DRV8841Config::DRV8841Config(PinName _Decay, PinName _nReset, PinName _nSleep, PinName _nFault)
  : Decay(_Decay, PIN_INPUT, PullNone, 0),
    nReset(_nReset, 0),
    nSleep(_nSleep, 0),
    nFault(_nFault, PullUp)
{
}

void DRV8841Config::setDecay(DecaySetting val)
{
  switch(val)
  {
    case DecaySetting::Slow:
      Decay.output();
      Decay = 0;
      break;
    case DecaySetting::Fast:
      Decay.output();
      Decay = 1;
      break;
    default:
    case DecaySetting::Mixed:
      Decay.input();
      break;
  }
}

}  // namespace drv8841
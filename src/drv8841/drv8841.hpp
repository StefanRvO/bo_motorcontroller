#pragma once
#include "FastPWM.h"
#include "mbed.h"
namespace drv8841
{
enum class MotorDirection {
  StopLow  = 0,
  Forward  = 1,
  Reverse  = 2,
  StopHigh = 3,
  Stop     = StopLow,
};

enum class MotorCurrent {
  CurrentFull = 0,
  Current38P  = 1,
  Current71P  = 2,
  CurrentStop = 3,
};

class BDCMotor {
 public:
  BDCMotor(PinName _In1, PinName _In2, PinName _Iset0, PinName _Iset1, float& speed);
  void setDirection(MotorDirection direction);
  void setCurrent(MotorCurrent current);
  void setPWMFreq(uint16_t freq)
  {
    In1.period_us(1000000. / freq);
    In2.period_us(1000000. / freq);
  }

  BDCMotor& operator=(MotorDirection direction)
  {
    setDirection(direction);
    return *this;
  }

 private:
  FastPWM    In1;
  FastPWM    In2;
  DigitalOut Iset0;
  DigitalOut Iset1;
  float&     speed;
};

enum class DecaySetting {
  Slow,
  Mixed,
  Fast,
};

class DRV8841Config {
 public:
  DRV8841Config(PinName _Decay, PinName _nReset, PinName _nSleep, PinName _nFault);
  bool fault() { return !nFault.read(); }
  void setSleep(bool val) { nSleep = !val; }
  void setReset(bool val) { nReset = !val; }
  void setDecay(DecaySetting val);

 private:
  DigitalInOut Decay;
  DigitalOut   nReset;
  DigitalOut   nSleep;
  DigitalIn    nFault;
};
}  // namespace drv8841
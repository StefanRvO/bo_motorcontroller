#include "BOSpeakerMotor.hpp"

#include <algorithm>
namespace speakermotor
{

BOSpeakerMotor::BOSpeakerMotor(drv8841::BDCMotor& _motor, std::vector<PinName> _startSwitches, std::vector<PinName> _endSwitches)
  : motor(_motor),
    target(BOSpeakerMotorTarget::STOP),
    startSwitch(_startSwitches),
    endSwitch(_endSwitches)
{
  motor.setCurrent(drv8841::MotorCurrent::CurrentFull);
}

bool BOSpeakerMotor::atStart()
{
  return startSwitch.isActive();
}

bool BOSpeakerMotor::atEnd()
{
  return endSwitch.isActive();
}

void BOSpeakerMotor::service()
{
  startSwitch.service();
  endSwitch.service();

  switch(target)
  {
    case BOSpeakerMotorTarget::START:
      if(atStart())
      {
        target = BOSpeakerMotorTarget::STOP;
        motor  = drv8841::MotorDirection::Stop;
      }
      else
      {
        motor = drv8841::MotorDirection::Reverse;
      }
      break;
    case BOSpeakerMotorTarget::END:
      if(atEnd())
      {
        target = BOSpeakerMotorTarget::STOP;
        motor  = drv8841::MotorDirection::Stop;
      }
      else
      {
        motor = drv8841::MotorDirection::Forward;
      }
      break;
    default:
    case BOSpeakerMotorTarget::STOP:
      motor = drv8841::MotorDirection::Stop;
      break;
  }
}

SwitchState::SwitchState(std::vector<PinName>& _switches)
{
  for(auto sw : _switches)
  {
    switches.push_back(DigitalIn(sw, PullNone));
  }
}

bool SwitchState::isActive()
{
  return timeActive >= HYSTERESIS_MS;
}

void SwitchState::service()
{
  if(std::any_of(switches.begin(), switches.end(), [](auto& sw) { return !sw; }))
  {
    timeActive++;
  }
  else
  {
    timeActive = 0;
  }
  if(timeActive > HYSTERESIS_MS)
  {
    timeActive = HYSTERESIS_MS;
  }
}

}  // namespace speakermotor
#pragma once
#include "drv8841/drv8841.hpp"
#include "mbed.h"
#include <vector>

namespace speakermotor
{

enum class BOSpeakerMotorTarget {
  START,
  END,
  STOP,
};

class SwitchState {
 public:
  SwitchState(std::vector<PinName>& _switches);
  void service();
  bool isActive();

 private:
  std::vector<DigitalIn>    switches;
  uint32_t                  timeActive    = 0;
  static constexpr uint32_t HYSTERESIS_MS = 150;
};

class BOSpeakerMotor {
 public:
  BOSpeakerMotor(drv8841::BDCMotor& _motor, std::vector<PinName> _startSwitches, std::vector<PinName> _endSwitches);

  bool atStart();
  bool atEnd();
  void service();

  BOSpeakerMotor& operator=(BOSpeakerMotorTarget val)
  {
    target = val;
    return *this;
  }

 private:
  drv8841::BDCMotor&   motor;
  BOSpeakerMotorTarget target;
  SwitchState          startSwitch;
  SwitchState          endSwitch;
  uint32_t             timeAtStart = 0;
  uint32_t             timeAtEnd   = 0;
};
}  // namespace speakermotor
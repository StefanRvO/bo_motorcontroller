#include "unknownpins.hpp"

#include "mbed.h"

typedef struct {
  DigitalInOut pin;
  uint8_t      val;
} pins_t;

static pins_t unKnownPins[] = {
  //   {.pin = DigitalInOut(P0_3, PIN_INPUT, PullNone, 0), .val = 1},
  //   {.pin = DigitalInOut(P0_4, PIN_INPUT, PullNone, 0), .val = 2},
  //   {.pin = DigitalInOut(P0_5, PIN_INPUT, PullNone, 0), .val = 3},
  //   {.pin = DigitalInOut(P0_6, PIN_INPUT, PullNone, 0), .val = 4},
  //   {.pin = DigitalInOut(P0_7, PIN_INPUT, PullNone, 0), .val = 5},
  //   {.pin = DigitalInOut(P0_8, PIN_INPUT, PullNone, 0), .val = 6},
  //   {.pin = DigitalInOut(P0_9, PIN_INPUT, PullNone, 0), .val = 7},
  //   {.pin = DigitalInOut(P0_10, PIN_INPUT, PullNone, 0), .val = 8},
  //   {.pin = DigitalInOut(P0_11, PIN_INPUT, PullNone, 0), .val = 9},
  //   {.pin = DigitalInOut(P0_12, PIN_INPUT, PullNone, 0), .val = 10},
  //   {.pin = DigitalInOut(P0_13, PIN_INPUT, PullNone, 0), .val = 11},
  //   {.pin = DigitalInOut(P0_14, PIN_INPUT, PullNone, 0), .val = 12},
  //   {.pin = DigitalInOut(P0_19, PIN_INPUT, PullNone, 0), .val = 13},
  //   {.pin = DigitalInOut(P0_20, PIN_INPUT, PullNone, 0), .val = 14},
  //   {.pin = DigitalInOut(P0_21, PIN_INPUT, PullNone, 0), .val = 15},
  //   {.pin = DigitalInOut(P0_23, PIN_INPUT, PullNone, 0), .val = 16},
  //   {.pin = DigitalInOut(P0_24, PIN_INPUT, PullNone, 0), .val = 17},
  //   {.pin = DigitalInOut(P0_25, PIN_INPUT, PullNone, 0), .val = 18},
  //   {.pin = DigitalInOut(P0_26, PIN_INPUT, PullNone, 0), .val = 19},
  //   {.pin = DigitalInOut(P0_27, PIN_INPUT, PullNone, 0), .val = 20},
  //   {.pin = DigitalInOut(P0_28, PIN_INPUT, PullNone, 0), .val = 21},
  //   {.pin = DigitalInOut(P0_29, PIN_INPUT, PullNone, 0), .val = 22},
  //   {.pin = DigitalInOut(P0_30, PIN_INPUT, PullNone, 0), .val = 23},
  //   {.pin = DigitalInOut(P1_0, PIN_INPUT, PullNone, 0), .val = 24},
  //   {.pin = DigitalInOut(P1_1, PIN_INPUT, PullNone, 0), .val = 25},
  //   {.pin = DigitalInOut(P1_4, PIN_INPUT, PullNone, 0), .val = 26},
  //   {.pin = DigitalInOut(P1_8, PIN_INPUT, PullNone, 0), .val = 27},
  //   {.pin = DigitalInOut(P1_10, PIN_INPUT, PullNone, 0), .val = 28},
  //   {.pin = DigitalInOut(P1_14, PIN_INPUT, PullNone, 0), .val = 29},
  //   {.pin = DigitalInOut(P1_15, PIN_INPUT, PullNone, 0), .val = 30},
  //   {.pin = DigitalInOut(P1_30, PIN_INPUT, PullNone, 0), .val = 33},
  //   {.pin = DigitalInOut(P1_31, PIN_INPUT, PullNone, 0), .val = 34},
  //   {.pin = DigitalInOut(P2_1, PIN_INPUT, PullNone, 0), .val = 36},
  //   {.pin = DigitalInOut(P2_2, PIN_INPUT, PullNone, 0), .val = 37},
  //   {.pin = DigitalInOut(P2_10, PIN_INPUT, PullNone, 0), .val = 38},
  //   {.pin = DigitalInOut(P4_28, PIN_INPUT, PullNone, 0), .val = 39},
  //   {.pin = DigitalInOut(P4_29, PIN_INPUT, PullNone, 0), .val = 40},
};

void output_pin(pins_t& pin)
{
  pin.pin.output();
  pin.pin = 1;
  wait_us(5);
  pin.pin = 0;
  wait_us(2);

  for(int8_t i = 0; i < 8; i++)
  {
    if((pin.val >> (7 - i)) & 1)
    {
      pin.pin = 1;
      pin.pin = 1;
      pin.pin = 1;
    }
    else
    {
      pin.pin = 1;
    }
    pin.pin = 0;
    wait_ns(250);
  }

  pin.pin = 1;
  pin.pin.input();
}

void output_pin_pull(pins_t& pin)
{
  pin.pin.input();
  pin.pin.mode(PullUp);
  wait_us(5);
  pin.pin.mode(PullDown);
  wait_us(1000);

  for(int8_t i = 0; i < 8; i++)
  {
    if((pin.val >> (7 - i)) & 1)
    {
      pin.pin.mode(PullUp);
      wait_us(200);
    }
    else
    {
      pin.pin.mode(PullUp);
      wait_us(50);
    }
    pin.pin.mode(PullDown);
    wait_us(500);
  }

  pin.pin.mode(PullUp);
  pin.pin.input();
}

void toggleUnknownPins()
{
  for(auto& pin : unKnownPins)
  {
    output_pin_pull(pin);
  }
}